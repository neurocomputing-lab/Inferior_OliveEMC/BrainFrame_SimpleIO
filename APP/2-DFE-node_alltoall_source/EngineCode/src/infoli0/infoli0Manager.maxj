package infoli0;


import com.maxeler.maxcompiler.v2.managers.BuildConfig;
import com.maxeler.maxcompiler.v2.managers.custom.CustomManager;
import com.maxeler.maxcompiler.v2.managers.custom.DFELink;
import com.maxeler.maxcompiler.v2.managers.custom.blocks.KernelBlock;
import com.maxeler.maxcompiler.v2.managers.custom.stdlib.DebugLevel;
import com.maxeler.maxcompiler.v2.managers.custom.stdlib.Max4MAIARingConnection;
import com.maxeler.maxcompiler.v2.managers.custom.stdlib.MaxRingBidirectionalStream;
import com.maxeler.maxcompiler.v2.managers.custom.stdlib.MemoryControlGroup;
import com.maxeler.maxcompiler.v2.managers.custom.stdlib.MemoryControlGroup.MemoryAccessPattern;
import com.maxeler.maxcompiler.v2.managers.engine_interfaces.CPUTypes;
import com.maxeler.maxcompiler.v2.managers.engine_interfaces.EngineInterface;
import com.maxeler.maxcompiler.v2.managers.engine_interfaces.EngineInterface.Direction;
import com.maxeler.maxcompiler.v2.managers.engine_interfaces.InterfaceParam;

public class infoli0Manager extends CustomManager {


	public infoli0Manager(infoliEngineParameters engineParams) {
		super(engineParams);

		KernelBlock  kernel  = addKernel(new infoli0Kernel(makeKernelParameters(s_kernelName)));


		config.setDefaultStreamClockFrequency(engineParams.getStreamFrequency());
		config.setEnableAddressGeneratorsInSlowClock(true);

		DebugLevel dbg = new DebugLevel(); //to add debugging annotations in maxdebug graphs
		dbg.setHasStreamStatus(true);
		debug.setDebugLevel(dbg);


		DFELink cpu2lmem = addStreamToOnCardMemory("cpu2lmem", MemoryControlGroup.MemoryAccessPattern.LINEAR_1D);
		DFELink lmem2cpu = addStreamFromOnCardMemory("lmem2cpu", MemoryControlGroup.MemoryAccessPattern.LINEAR_1D);

		DFELink fromcpu = addStreamFromCPU("fromcpu");
		DFELink tocpu = addStreamToCPU("tocpu");

		cpu2lmem <== fromcpu;
		tocpu <== lmem2cpu;


		MaxRingBidirectionalStream maxRingStream1 = addMaxRingBidirectionalStream(
				"maxRingStream0", Max4MAIARingConnection.MAXRING_LOCAL_TOP);





		//links for DFE0
		DFELink ini0 = addStreamFromOnCardMemory("Ini0", MemoryAccessPattern.LINEAR_1D);
		DFELink iApp0 = addStreamFromOnCardMemory("iApp0", MemoryAccessPattern.LINEAR_1D);
		DFELink IC0 = addStreamFromOnCardMemory("IC0", MemoryAccessPattern.LINEAR_1D);
		DFELink DendIn1 = maxRingStream1.getLinkFromRemoteDFE();


		kernel.getInput("Ini0").connect(ini0);
		kernel.getInput("iApp0").connect(iApp0);
		kernel.getInput("IC0").connect(IC0);
		kernel.getInput("DendIn1").connect(DendIn1);

		DFELink s0 = addStreamToOnCardMemory("s0", MemoryAccessPattern.LINEAR_1D);
		DFELink DendOut1 = maxRingStream1.getLinkToRemoteDFE();



		s0.connect(kernel.getOutput("s0"));
		DendOut1.connect(kernel.getOutput("DendOut1"));
		//DFELink toMaxring = kernel.getOutput("DendOut1");

		//DendOut1.connect(toMaxring);

		//_CustomManagers.setStreamBufferSpaceRequirement(toMaxring, 32 * 7680);

	}




	static final int CellParams = 24;
	//static final int UnrollFactor =16;  //Make sure this has the same value in the kernel code and manager (UnrollFactor)
	static final int numEngines = 2;

	private static final String s_kernelName = "infoli0Kernel";

	public static void main(String[] args) {
		infoliEngineParameters params = new infoliEngineParameters(args);
	CustomManager manager = new infoli0Manager(params);


		manager.createSLiCinterface(interfaceDefault());
		configBuild(manager, params);

		//manager.createSLiCinterface(interfaceWrite0("writeLMem0"));
		//manager.createSLiCinterface(interfaceRead0("readLMem0"));



		manager.build();
	}

/*	private static EngineInterface interfaceWrite0(String name) {
		EngineInterface ei = new EngineInterface(name);
		CPUTypes TYPE = CPUTypes.INT64;
		CPUTypes   type_float = CPUTypes.FLOAT;
		InterfaceParam start = ei.addParam("start", TYPE);
		InterfaceParam size  = ei.addParam("size", TYPE);

		InterfaceParam sizeInBytes = size * type_float.sizeInBytes();

		ei.setStream("fromcpu", type_float, sizeInBytes );
		ei.setLMemLinear("cpu2lmem", start* type_float.sizeInBytes() , sizeInBytes);
		ei.ignoreAll(Direction.IN_OUT);
		return ei;
	}

	private static EngineInterface interfaceRead0(String name) {
		EngineInterface ei = new EngineInterface(name);
		CPUTypes TYPE = CPUTypes.INT64;
		CPUTypes   type_float = CPUTypes.FLOAT;
		InterfaceParam start = ei.addParam("start", TYPE);
		InterfaceParam size  = ei.addParam("size", TYPE);

		InterfaceParam sizeInBytes = size * type_float.sizeInBytes();

		ei.setLMemLinear("lmem2cpu", start* type_float.sizeInBytes(), sizeInBytes);
		ei.setStream("tocpu", type_float, sizeInBytes);
		ei.ignoreAll(Direction.IN_OUT);
		return ei;
	}*/

	private static EngineInterface interfaceDefault() {
		EngineInterface engine_interface = new EngineInterface();
		CPUTypes   type = CPUTypes.INT64;
		CPUTypes   type_float = CPUTypes.FLOAT;
		//int        size = type.sizeInBytes();
		int        float_size = type_float.sizeInBytes();

		InterfaceParam  N_size   = engine_interface.addParam("N_size", type);
		InterfaceParam  simsteps    = engine_interface.addParam("simsteps", type);
		InterfaceParam  time_mux_factor    = engine_interface.addParam("time_mux_factor", type);
		InterfaceParam  Max_N_Size    = engine_interface.addParam("Max_N_Size", type);
		InterfaceParam  DFENo    = engine_interface.addParam("DFENo", type);

		InterfaceParam  zero = engine_interface.addConstant(0l);

		//Scalar inputs
		engine_interface.setScalar(s_kernelName, "N_size", N_size);
		engine_interface.setScalar(s_kernelName, "simsteps", simsteps);
		engine_interface.setScalar(s_kernelName, "time_mux_factor", time_mux_factor);
		engine_interface.setScalar(s_kernelName, "Max_N_Size", Max_N_Size);
		engine_interface.setScalar(s_kernelName, "DFENo", DFENo);

		//Input Traces from LMEM for DFE0
		engine_interface.setLMemLinear("Ini0", zero, CellParams*N_size * float_size);
		engine_interface.setLMemLinear("iApp0", ((CellParams*(Max_N_Size) * float_size)), N_size*simsteps* float_size);
		engine_interface.setLMemLinear("IC0",(((CellParams*(Max_N_Size) * float_size) + ((Max_N_Size)*simsteps* float_size)) + (Max_N_Size* float_size)) ,N_size* float_size);


		//Output To LMEM for DFE0
		engine_interface.setLMemLinear("s0",((CellParams*(Max_N_Size) * float_size) + ((Max_N_Size)*simsteps* float_size)) ,N_size*simsteps* float_size);



		engine_interface.ignoreAll(Direction.IN_OUT);
		return engine_interface;
	}






	private static void configBuild(CustomManager manager, infoliEngineParameters params) {
		BuildConfig buildConfig = manager.getBuildConfig();
		buildConfig.setMPPRCostTableSearchRange(params.getMPPRStart(), params.getMPPREnd());
		buildConfig.setMPPRParallelism(params.getMPPRNumThreads());
		buildConfig.setMPPRRetryNearMissesThreshold(params.getMPPRRetryThreshold());
	}
}



